import axios, { AxiosError } from 'axios'
import fs from 'fs';
import csv from 'csv-parser'
import { exit } from 'process';

const API_KEY = 'REPLACE_ME';
 
const args = process.argv.slice(2);
if(!args[0]) {
  console.error('Please enter the path to the properties file.');
  exit();
}
let index = 0;
fs.createReadStream(args[0])
  .pipe(csv({separator: ';'}))
  .on('data', (row) => {
      setTimeout(() => {
        const body = {
          'name': row['Internal name'],
          'label': row['Name'],
          'description': row['Description'],
          'groupName': row['Group name'],
          'type': row['Type'],
          'formField': row['Form field'].toLowerCase(),
          'options': row['Options'] ? JSON.parse(row['Options']) : []
        }
        postProperty(body)
          .then()
          .catch((e: AxiosError) => {
            console.error(e.response?.data)
            if((e.response?.data as any).message.match(/property group [^\s]* does not exist/)){
              console.log(`creating new property group ${body.groupName}`);
              axios.post(`https://api.hubapi.com/properties/v1/contacts/groups?hapikey=${API_KEY}`, {
                name: body.groupName,
                displayName: body.groupName
              }).then(() => postProperty(body).then()).catch(e => console.error(e));
            }
          });
      }, index*400);
      index++;
          
  })
  .on('end', () => {
    console.log('CSV file successfully processed');
  });

const postProperty = (body: any) => 
  axios.post(`https://api.hubapi.com/properties/v1/contacts/properties?hapikey=${API_KEY}`, body)
