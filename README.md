## Syncing contact properties

1. On the production Hubspot account, go to Settings > Data Management > Properties > Export all properties. Download the CSV file. The file will have multiple sheets originally, make sure to delete all sheets except the 'Contacts' sheet.

2. Get an API key from the Quandoo Dev Account by going to Settings > Integrations > API key. Replace that key in the `updateProperties.ts` file.

3. Use the command `npm run update path-to-contact-properties-file`. This will send each contact property to the development Hubspot account. It might take a while because there is a timeout between each API request to avoid getting rate-limited. You can play around with this number; probably it could be a bit faster :)


Note that if a property with the same `name` already exists, the request will fail; the existing property will not be updated. 
